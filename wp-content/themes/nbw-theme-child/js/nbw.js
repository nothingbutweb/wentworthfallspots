/**

* Place to put custom JS for NBW

*

*/

var alm_is_animating = false;

function alm_adv_filter(){

    if(alm_is_animating){
        return false; // Exit if filtering is still active 
    }
    
    alm_is_animating = true;
    
    var obj= {}, 
         count = 0;
    
    // Loop each filter menu
    jQuery('.advanced-filter-menu').each(function(e){

        var menu = jQuery(this),
            type = menu.data('type'), // type of filter elements (checkbox/radio/select)
            parameter = menu.data('parameter'), // category/tag/year
            value = '',
            count = 0;
    
        if(type === 'select'){ // Select
            var select = jQuery(this);
            value += select.val();
        }
    
        obj[parameter] = value; // add value(s) to obj
        
    });
    
    console.log(obj);
    
    var data = obj;      
    // // Send data to Ajax Load More
}
    
/*
* almFilterComplete()
* Callback function sent from core Ajax Load More
*
*/
jQuery.fn.almFilterComplete = function(){      
    alm_is_animating = false; // clear animating flag
};


jQuery(document).ready(function(){

    // popup for header mailchimp click
    jQuery("a#inline").fancybox({
        // 'hideOnContentClick': true
    });

    // same height for title in homepage
    jQuery('.box-category .header-title').matchHeight();
    jQuery('.name.product-title').matchHeight();


    jQuery(document).on( 'change', 'form.filterform select', function() {

        var obj= {};

        var select = jQuery(this);

        var gallery_id = jQuery(this).val();
       
        var content = jQuery('.gallery-content');

        jQuery.ajax({
            type : 'post',
            url : '/wp-admin/admin-ajax.php',
            data : {
                action : 'load_gallery_images',
                gallery_id : gallery_id
            },
            beforeSend: function() {
                select.prop('disabled', true);
                content.addClass('loading');
            },
            success : function( response ) {

                
                obj['post_id'] = gallery_id; // add value(s) to obj

                var data = obj;

                console.log(data);

                jQuery.fn.almFilter('fade', '300', data);

                jQuery('.alm-reveal').addClass('row');

                select.prop('disabled', false);
                content.removeClass('loading');
                content.html( response );
            }
        });

        return false;
                
                
    });



    function guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return 'cus' + s4() + s4()  + s4() + s4() +
            s4() + s4() + s4() + s4();
    }

    jQuery(window).resize(function () {
        if ( jQuery( window ).width() < 480 ){
            changePesudeForMobile();
        }
    });
    
    function changePesudeForMobile() {

        var obj = obj || {};

        obj.titles = [];

        jQuery('#tab-description table tbody tr').each(function (index, value) {

            if (index === 0){
                jQuery(this).find('th').each(function(key, subValue){
                    obj.titles.push({ index: key, title: jQuery(subValue).html(), guid: guid()});
                });
            } else {
                jQuery(this).find('td').each(function(key, subValue){
                    jQuery(this).addClass(obj.titles[key].guid);
                });
            }
        });

        var str = '';
        jQuery.each(obj.titles, function (key, value) {
            str += '.' + value.guid + '::before{content:"' + value.title  + '" !important; display:none;}';
        });
        jQuery('<style>'+str+'</style>').appendTo('head');
    }



});