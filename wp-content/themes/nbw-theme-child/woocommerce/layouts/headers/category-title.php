<!--Overwrite category-title file from child theme-->



<?php



function show_cat_title(){

    ob_start();
    if(single_cat_title('', false) == ""){
        echo "Our Products";
    } else {
        echo single_cat_title();
    }
    return ob_get_clean();
}



function show_cat_description(){
    ob_start();
    $catDescription = get_the_archive_description();
    echo $catDescription;
    return ob_get_clean();
}



echo do_shortcode('

    [section padding="0px"]
        [row h_align="center"]
            [col span__sm="12"]
                [gap height="40px"]
            [/col]
            [col span="10" span__sm="12" align="center"]
                <div class="big-header">'. show_cat_title() .'</div>
            [/col]
        [/row]
    [/section]
');



?>



<!--<div class="shop-page-title category-page-title page-title --><?php //flatsome_header_title_classes() ?><!--">-->

<!--    <div class="page-title-inner flex-row  medium-flex-wrap container">-->

<!--        <div class="flex-col flex-grow medium-text-center">-->

<!--            --><?php //do_action('flatsome_category_title') ;?>

<!--        </div>-->

<!--        <div class="flex-col medium-text-center">-->

<!--            --><?php //do_action('flatsome_category_title_alt') ;?>

<!--        </div>-->

<!--    </div>-->

<!--</div>-->

