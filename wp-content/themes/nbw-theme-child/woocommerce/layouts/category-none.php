<div class="row category-page-row">

		

		<?php echo do_shortcode('[divider width="100%" height="1px" margin="30px" color="rgb(204, 204, 204)"]'); ?>
		<div class="col large-8 small-12 pb-0 pb-30-mob"><?php dynamic_sidebar('shop-sidebar'); ?></div>
		<div class="col large-4 small-12 pb-0"><?php echo do_shortcode('[search]'); ?></div>
		<?php echo do_shortcode('[divider width="100%" height="1px" margin="20px" color="rgb(204, 204, 204)"]'); ?>

		<div class="col large-12">
		<?php
			 do_action('flatsome_products_before');

			/**
			 * woocommerce_before_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 */
			do_action( 'woocommerce_before_main_content' );
		?>

		<?php do_action( 'woocommerce_archive_description' ); ?>

		<?php if ( have_posts() ) : ?>

			<?php
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

		<?php
			 do_action('flatsome_products_after');
			/**
			 * woocommerce_after_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
		?>

            <?php
            $cate = get_queried_object();
            if ($cate->term_id === 29){?>

                <div class="custom-header-related">
                    <div class="big-header text-center">Lightweight Terrazzo Accessories</div>
                </div>

            <?php
                echo do_shortcode('[section padding="0px"]
                    [row h_align="center"]
                            [product_category columns="3" category="lightweight-terrazzo-accessories"]
                    [/row]
                [/section]');
            }

            ?>

		<div class="custom-header-related">
			<div class="big-header text-center"><?php echo show_cat_title(); ?> Gallery</div>
		</div>

		<?php 

		$term_name = $cate->name;

		$gallery_id = $wpdb->get_var( "SELECT ID FROM $wpdb->posts WHERE post_title = '".$term_name."' " );

		echo do_shortcode('[ajax_load_more repeater="template_31" posts_per_page="6" scroll="false" button_label="Load More Images" button_loading_label="Loading..." seo="true" acf="true" acf_field_type="gallery" acf_field_name="gallery" seo="true" container_type="div" transition="fade" images_loaded="true" acf_post_id="'. $gallery_id .'"]');

		?>

		</div><!-- .large-12  -->
</div><!-- .row -->
<?php

echo do_shortcode('
        
            [section padding="0px"]
                [row h_align="center"]
                    [col span="10" span__sm="12" align="center"]
                        <div class="term-desc">'. get_the_archive_description() .'</div>
                    [/col]
                [/row]
            [/section]
        ');

?>