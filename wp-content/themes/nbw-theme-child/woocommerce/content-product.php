<?php

/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.0.0
 */


if (!defined('ABSPATH')) {

    exit; // Exit if accessed directly

}


global $product;


// Ensure visibility

if (empty($product) || !$product->is_visible()) {

    return;

}


// Check stock status

$out_of_stock = get_post_meta($post->ID, '_stock_status', true) == 'outofstock';


// Extra post classes

$classes = array();

$classes[] = 'product-small';

$classes[] = 'col';

$classes[] = 'has-hover';


if ($out_of_stock) $classes[] = 'out-of-stock';


?>


<div <?php post_class($classes); ?>>

    <div class="col-inner">


        <?php do_action('woocommerce_before_shop_loop_item'); ?>

        <div class="product-small box <?php echo flatsome_product_box_class(); ?>">

            <div class="box-image pad-20 custom-box-image">

                <div class="<?php echo flatsome_product_box_image_class(); ?>">

                    <a href="<?php echo get_the_permalink(); ?>">

                        <?php

                        /**
                         *
                         * @hooked woocommerce_get_alt_product_thumbnail - 11
                         * @hooked woocommerce_template_loop_product_thumbnail - 10
                         */

                        do_action('flatsome_woocommerce_shop_loop_images');

                        ?>

                    </a>

                </div>

                <div class="image-tools is-small top right show-on-hover">

                    <?php do_action('flatsome_product_box_tools_top'); ?>

                </div>

                <div class="image-tools is-small hide-for-small bottom left show-on-hover">

                    <?php do_action('flatsome_product_box_tools_bottom'); ?>

                </div>

                <div class="image-tools <?php echo flatsome_product_box_actions_class(); ?>">

                    <?php do_action('flatsome_product_box_actions'); ?>

                </div>

                <?php if ($out_of_stock) { ?>
                    <div class="out-of-stock-label"><?php _e('Out of stock', 'woocommerce'); ?></div><?php } ?>

            </div><!-- box-image -->


            <div class="box-text <?php echo flatsome_product_box_text_class(); ?>">

                <div class="custom-title-product text-center mar-bot-10">

                    <div class="title-wrapper small-header letter-space-3">

                        <?php do_action('woocommerce_shop_loop_item_title'); ?>

                    </div>

                </div>

                <?php

                do_action('woocommerce_before_shop_loop_item_title');


                echo '<div class="price-wrapper custom-price-wrapper"><span class="price-label">PRICE</span>';

                do_action('woocommerce_after_shop_loop_item_title');

                echo '</div>';


                ?>


                <div class="custom-box-product-after">

                    <?php do_action('flatsome_product_box_after'); ?>

                </div>


                <?php //echo do_shortcode('[divider width="100px" height="1px" margin="30px" color="rgb(204, 204, 204)"]'); ?>


                <div class="custom-see-product mar-bot-20">

                    <a class="button primary is-larger" href="<?php echo get_the_permalink(); ?>">Click for Details</a>

                </div>


            </div><!-- box-text -->

        </div><!-- box -->

        <?php do_action('woocommerce_after_shop_loop_item'); ?>

    </div><!-- .col-inner -->

</div><!-- col -->

