<?php

/**
 * Overwrite category template file.
 *
 * @package flatsome
 */


get_header();


function show_cat_title()
{

    ob_start();


    echo single_cat_title();


    return ob_get_clean();

}


function show_cat_description()
{

    ob_start();


    the_archive_description();


    return ob_get_clean();

}


function get_cat_slug()
{

    ob_start();


    $catID = get_cat_ID(show_cat_title());

    $cat_id = (int)$catID;

    $category = &get_category($cat_id);

    echo $category->slug;


    return ob_get_clean();

}


// If you want to change the image background in blog header, please change the "280" in section shortcode


/*echo do_shortcode('

    [section bg="280" bg_size="orginal" padding="100px"]

    

    [row v_align="middle" h_align="right"]

    

    [col span="5" span__sm="12" span__md="6" align="center"]

    

    <div class="big-header white-text">'. show_cat_title() .'</div>

    

    [divider width="80px" height="1px"]

    

    [/col]

    

    [/row]

    

    [/section]

    

    [section padding="80px"]



    [row h_align="center"]

    

    [col span="10" span__sm="12" align="center"]' . show_cat_description() . '[/col]

    

    [/row]

    [row]

    

    [col span="6" span__sm="12" class="no-pad-bot"]

    

    [all-categories-list]

    

    [/col]

    [col span="6" span__sm="12" padding="0px 20px 0px 0px" class="custom-search-bar"]

    

    [search]

    

    

    [/col]

    

    [/row]

    [row]

    

    [col span__sm="12"]

    

    <p>[ajax_load_more container_type="div" css_classes="blog-ajax" post_type="post" posts_per_page="4" scroll="false" button_label="Load More" button_loading_label="Loading" category="'. get_cat_slug() .'"]</p>

    

    [/col]

    

    [/row]

    

    [/section]



');
*/


get_footer();

