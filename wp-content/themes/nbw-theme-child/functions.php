<?php

// Add custom Theme Functions here



add_filter('jpeg_quality', function($arg){return 75;});



function _remove_script_version( $src ){

    $parts = explode( '?ver', $src );

    return $parts[0];

}

add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );

add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );


/** Add custom script **/

function nbw_script() {

    wp_enqueue_script('jquery');

    wp_enqueue_script( 'geocomplete-script', get_stylesheet_directory_uri() . '/js/jquery.geocomplete.min.js');

    wp_enqueue_script( 'sameheight-script', get_stylesheet_directory_uri() . '/js/jquery.sameHeight.min.js', array('jquery'));
    wp_enqueue_script( 'nbw-script', get_stylesheet_directory_uri() . '/js/nbw.js', array('jquery', 'sameheight-script'));

}

add_action( 'wp_enqueue_scripts', 'nbw_script' );

/** Add custom script end **/


/** Shortcode to show menu **/

function print_menu_shortcode($atts) {

    ob_start();

    $a = shortcode_atts( array(

        'id' => 'Footer Menu'

    ), $atts );

    wp_nav_menu( array( 'menu' => $a['id'] ));

    return ob_get_clean();

}

add_shortcode('menu', 'print_menu_shortcode');


/** Shortcode to show menu end **/

/** Shortcode to show shopping cart **/

function show_shopping_cart(){

    ob_start(); ?>

    <div class="flex-col cart-custom">

        <ul>

            <?php get_template_part('template-parts/header/partials/element','cart'); ?>

        </ul>

    </div>

    <?php

    return ob_get_clean();

}

add_shortcode('show-shopping-cart', 'show_shopping_cart');


/** Shortcode to show shopping cart end **/

/** Shortcode to change excerpt length **/

function wpdocs_custom_excerpt_length( $length ) {

    return 16;

}

add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

/** Shortcode to change excerpt length end **/

/** Shortcode to show all list of categories **/

function all_categories_list() {

    ob_start(); ?>



    <div class="cat-dropdown-custom">

        <?php wp_dropdown_categories( 'show_option_none=Filter By' ); ?>

        <script type="text/javascript">

            var dropdown = document.getElementById("cat");

            function onCatChange() {

                if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {

                    location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;

                }

            }

            dropdown.onchange = onCatChange;

        </script>

    </div>



    <?php

    return ob_get_clean();

}

add_shortcode( 'all-categories-list', 'all_categories_list');


/** Shortcode to show all list of categories end **/


/** Shortcode to login/logout link **/

function login_logout(){

    ob_start();

    if ( is_user_logged_in() ) { ?>

        <a href="<?php echo wp_logout_url('/logout/'); ?>">Logout</a>

    <?php } else { ?>

        <a href="/my-account">Login / Register</a>

    <?php }

    return ob_get_clean();

}

add_shortcode('login-logout', 'login_logout');


/** Shortcode to login/logout link end **/

add_filter("gform_submit_button_1", "form_submit_button", 10, 2);
function form_submit_button($button, $form){
return "<button class='button' id='gform_submit_button_{$form["id"]}'><span><i class='icon icon-envelop'></i> Submit Enquiry Form</span></button>";
}

function display_gallery_images($atts){

    ob_start();

    ?>
    <div class="row align-center">
        <div class="col large-4 small-12">
            <form method="get" class="filterform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
                <?php $query = new wp_query(array(
                    'post_type' => 'product_gallery', 
                    'posts_per_page' => -1
                )); ?>
                
                <select class="advanced-filter-menu" data-type="select" data-parameter="post_id">
                    <?php
                        if($query->have_posts()){
                           while($query->have_posts()){
                                $query->the_post();
                                global $post;
                                $post_id=$post->ID;
                                ?><option value="<?php echo $post_id; ?>"><?php the_title(); ?></option><?php
                            }
                        }
                    ?>
                </select>
            </form>
            
        </div>
    </div>

    <div class="row gallery-content"><?php echo do_shortcode('[ajax_load_more posts_per_page="6" scroll="false" button_label="Load More Images" button_loading_label="Loading..." seo="true" acf="true" acf_field_type="gallery" acf_field_name="gallery" seo="true" container_type="div" transition="fade" images_loaded="true" acf_post_id="8118"]'); ?></div>


  <?php

  return ob_get_clean();

}
add_shortcode('product-gallery', 'display_gallery_images');


function load_gallery_images(){
    
    ob_start();

    $gallery_id = $_POST['gallery_id'];

    echo do_shortcode('[ajax_load_more repeater="template_31" posts_per_page="6" scroll="false" button_label="Load More Images" button_loading_label="Loading..." seo="true" acf="true" acf_field_type="gallery" acf_field_name="gallery" seo="true" container_type="div" transition="fade" images_loaded="true" acf_post_id="'. $gallery_id .'"]');

    $content = ob_get_clean();

    echo $content;

    die();
}

add_action( 'wp_ajax_load_gallery_images', 'load_gallery_images' );

add_action( 'wp_ajax_nopriv_load_gallery_images', 'load_gallery_images' );

add_filter( 'woocommerce_is_purchasable', false );

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

//    unset( $tabs['description'] );          // Remove the description tab
    unset( $tabs['reviews'] );          // Remove the reviews tab
    unset( $tabs['additional_information'] );   // Remove the additional information tab

    $tabs['description'] = array(
        'title'    => __( 'Description', 'woocommerce' ),
        'priority' => 10,
        'callback' => 'woocommerce_product_description_tab',
    );

    return $tabs;

}

add_filter( 'woocommerce_is_purchasable', false );


function wecodeart_theme_mods() {
    set_theme_mod('search_placeholder', 'Search Product...');
}

add_action( 'init', 'wecodeart_theme_mods' );


function my_filter_plugin_updates( $value ) {
    if( isset( $value->response['product-enquiry-for-woocommerce/product-enquiry-for-woocommerce.php'] ) ) {
        unset( $value->response['product-enquiry-for-woocommerce/product-enquiry-for-woocommerce.php'] );
    }
    return $value;
}
add_filter( 'site_transient_update_plugins', 'my_filter_plugin_updates' );

function my_post_queries( $query ) {
    // do not alter the query on wp-admin pages and only alter it if it's the main query
    if (! is_admin() ){
        $query->set( 'posts_per_page', -1 );
    }
}
add_action( 'pre_get_posts', 'my_post_queries' );

function woocommerce_product_description_tab() {
    global $product;

//    if ( get_product(get_the_ID())->is_type( 'variable' )) {
    if ( $product->is_type( 'variable' )) {
//        $variations = get_product(get_the_ID())->get_available_variations();
        $variations = $product->get_available_variations();
        $varnum = count($variations);

//        var_dump($varnum);

        if ($varnum >= 1) { ?>
            <table>
                <tr>
                    <th>Code</th>
                    <th>Size(cm) L x W x H</th>
                    <th>Description</th>
                    <th>Approx Weight (KG)</th>
                    <th>Wholesale Prices</th>
                </tr>
                <?php
                for ($i = 0; $i < $varnum; $i++) {
                    echo '<pre  class="testing" style="display: none;">';
                    var_dump($variations[$i]);
                    echo '</pre>';
                    ?>
                    <tr>
                        <td><?php echo $variations[$i]['sku']; ?></td>
                        <td><?php echo $variations[$i]['dimensions_html']; ?></td>
                        <td><?php echo $variations[$i]['variation_description']; ?></td>
                        <td><?php echo $variations[$i]['weight']; ?></td>
                        <td><?php echo $variations[$i]['price_html']?: $product->get_price_html();; ?></td>
                    </tr>
                    <?php
                } ?>
            </table>
            <?php
        } else {
            the_content();
        }
    } else {
            the_content();
    }

}
