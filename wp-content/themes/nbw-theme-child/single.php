<?php
/**
 * Overwrite single.php to create design for express site.
 *
 * @package flatsome
 */

get_header();

?>

<!-- This is the default Flatsome code for single.php -->

<!--<div id="content" class="blog-wrapper blog-single page-wrapper">-->
<?php //get_template_part( 'template-parts/posts/layout', get_theme_mod('blog_post_layout','right-sidebar') ); ?>
<!--</div>-->

<!-- This is the end of default Flatsome code for single.php -->

<?php wp_reset_postdata(); ?>

<section class="breadcrumb-section">
    <div class="row">
        <div class="col large-12 small-12 no-pad-bot white-text">
            <p class="no-mar-bot"><a href="/">Home</a> / <a href="/category/blog/">Blog</a> / <?php the_title(); ?></p>
        </div>
    </div>
</section>
<section class="inner-blog-section">
    <div class="row">
        <div class="col large-6 small-12">
            <?php the_post_thumbnail(); ?>
        </div>
        <div class="col large-6 small-12 no-pad-bot">
            <div class="content-column">
                <div class="big-header mar-bot-20"><?php the_title(); ?></div>
                <div class="small-header letter-space-3"><?php echo get_the_date(); ?></div>
                <?php
                echo do_shortcode('[divider width="100px" height="1px" margin="30px" color="rgb(204, 204, 204)"]');
                the_content();
                echo do_shortcode('[divider width="100px" height="1px" margin="30px" color="rgb(204, 204, 204)"]');
                echo do_shortcode('[share style="outline"]');
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col large-12 small-12">
            <?php
                echo do_shortcode('[divider width="100%" height="1px" margin="30px" color="rgb(204, 204, 204)"]');
            ?>
            <div class="navigation-blog">
                <div class="alignleft">
                    <div class="grey-button"><?php previous_post_link( '%link','Previous Post' ) ?></div>
                </div>
                <div class="alignright">
                    <div class="grey-button"><?php next_post_link( '%link','Next Post' ) ?></div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
