<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main><!-- #main -->

<footer id="footer" class="footer-wrapper">

    <?php do_action('flatsome_footer'); ?>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<script>
    document.addEventListener( 'wpcf7mailsent', function( event, real ) {

        // this is for the mailchimp form
        if (event.detail.id === 'wpcf7-f155-o1'){
            location = '/thank-you-for-subscription';
        } else {
            // this is for the rest form.
            location = '/thank-you';
        }
    }, false );
</script>

<?php wp_footer(); ?>

<div style="display: none;" >
    <div class="cus-popup-container" id="hidden-content" style="max-width: 600px;">
        <?php echo do_shortcode('[contact-form-7 id="155" title="Subscribe Form"]');?>
    </div>
</div>

</body>
</html>

<script>
    
 jQuery(document).ready(function(){
      var regx = /^[A-Za-z]/;
     
      jQuery('.contact-send').click(function(){
        var valname = jQuery('#contact-name').val();
         
        if (!regx.test(valname)) 
      {
       
   		jQuery("#errors").html("<div id='cont-name'>Name should be only charachter</div>");
   	
          
      }
     else if(regx.test(valname))
     {
         
          jQuery("#cont-name").hide();
     }
       });
       
        jQuery('#contact-phone').keypress(function(key) {
        if(key.charCode < 48 || key.charCode > 57) return false;
     
  });
 });
  
</script>
